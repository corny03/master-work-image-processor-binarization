package com.zajac.adam.processing;

import com.zajac.adam.data.ImageDatabaseDAO;
import com.zajac.adam.data.ImageEntity;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by zajac on 24.11.2017.
 * Processing thread test
 */
public class ProcessingThreadTest {

    @Mock
    private ImageDatabaseDAO imageDatabaseDAO;

    @Before
    public void setUp() throws Exception {
        imageDatabaseDAO = Mockito.mock(ImageDatabaseDAO.class);
    }

    @Test
    public void run() throws Exception {
        //given
        Date sqlDate = new Date(Calendar.getInstance().getTime().getTime());
        String fileName = "lena.jpg";
        byte[] bytes = Files.readAllBytes(Paths.get(fileName));
        String id = "5L";
        ImageEntity processingImageEntity = new ImageEntity(id, fileName, "Test image", null, sqlDate,
                Status.PROCESSING.toString());
        ProcessingThread processingThread = new ProcessingThread(processingImageEntity, bytes, imageDatabaseDAO);
        //when
        processingThread.run();
        //then
        assertEquals(processingImageEntity.getStatus(), Status.DONE.toString());
        assertTrue(processingImageEntity.getBytes() != null);
    }
}