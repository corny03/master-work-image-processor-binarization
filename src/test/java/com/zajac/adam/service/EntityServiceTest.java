package com.zajac.adam.service;

import com.zajac.adam.data.DataSource;
import com.zajac.adam.data.ImageDatabaseDAO;
import com.zajac.adam.data.ImageEntity;
import com.zajac.adam.processing.Status;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Date;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

/**
 * Created by zajac on 18.01.2018.
 * Service test
 */
public class EntityServiceTest {

    private EntityService entityService;
    @Value("${spring.application.name}")
    private String appName;
    @Mock
    private DataSource dataSource;
    @Mock
    private ImageDatabaseDAO imageDatabaseDAO;

    @Before
    public void setUp() throws Exception {
        this.dataSource = Mockito.mock(DataSource.class);
        this.imageDatabaseDAO = Mockito.mock(ImageDatabaseDAO.class);
        this.entityService = new EntityService(this.dataSource, this.imageDatabaseDAO);
    }

    @Test
    public void getImageEntity() throws Exception {
        //given
        String id = "1L", idTwo = "2L", idThree = "3L", idFour = "4L";
        byte[] bytes = {1, 23, 4, 53, 12, 32, 98, 67};
        Date sqlDate = new Date(Calendar.getInstance().getTime().getTime());
        final String filename = "img.jpg";
        String thisServiceId = appName + '-' + id;
        ImageEntity goodImageEntity = new ImageEntity(thisServiceId, filename, "some",
                bytes, sqlDate, Status.DONE.toString());
        ImageEntity processingImageEntity = new ImageEntity(thisServiceId, filename, "some",
                null, sqlDate, Status.PROCESSING.toString());
        final ByteArrayResource byteArrayResource = new ByteArrayResource(bytes, filename);
        ResponseEntity<ByteArrayResource> responseWhenIsOk =
                ResponseEntity.status(HttpStatus.OK).header("some", "some").header("content-disposition", "filename=\"img.jpg\"").body(byteArrayResource);
        //when
        Mockito.when(this.imageDatabaseDAO.findById(thisServiceId)).thenReturn(goodImageEntity);
        Mockito.when(this.dataSource.getImage(idTwo)).thenReturn(responseWhenIsOk);
        Mockito.when(this.imageDatabaseDAO.save(any(ImageEntity.class))).thenReturn(processingImageEntity);
        //then
        assertEquals(goodImageEntity, entityService.getImageEntity(id));
        assertEquals(processingImageEntity, entityService.getImageEntity(idTwo));
    }

    /**
     * @throws Exception when problem with database
     */
    @Test
    public void uploadToDatabase() throws Exception {
        //given
        String id = "1L", idTwo = "2L", idThree = "3L", idFour = "4L";
        byte[] bytes = {1, 23, 4, 53, 12, 32, 98, 67};
        Date sqlDate = new Date(Calendar.getInstance().getTime().getTime());
        final String filename = "img.jpg";
        ImageEntity processingImageEntity = new ImageEntity(id, filename, "some",
                null, sqlDate, Status.PROCESSING.toString());
        //when
        Mockito.when(this.imageDatabaseDAO.save(any(ImageEntity.class))).thenReturn(processingImageEntity);
        //then
        assertEquals(processingImageEntity, entityService.uploadToDatabase(id, filename, bytes));

    }

    @Test
    public void deleteImage() throws Exception {
        //given
        String id = "1L", idTwo = "2L";
        String thisServiceId = appName + '-' + id;
        //when
        entityService.deleteImage(id);
        entityService.deleteImage(idTwo);
        //then
        Mockito.verify(this.imageDatabaseDAO, Mockito.times(1)).delete(thisServiceId);
    }

    @Test
    public void deleteAllImagesWhatContainInId() throws Exception {
        //given
        String id = "1L", idTwo = "2L";
        //when
        entityService.deleteAllImagesWhatContainInId(id);
        entityService.deleteAllImagesWhatContainInId(idTwo);
        //then
        Mockito.verify(this.imageDatabaseDAO, Mockito.times(1)).deleteAllByIdLike("%" + id);
    }

}