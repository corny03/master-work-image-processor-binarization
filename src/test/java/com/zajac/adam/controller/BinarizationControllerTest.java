package com.zajac.adam.controller;

import com.zajac.adam.data.ImageEntity;
import com.zajac.adam.processing.Status;
import com.zajac.adam.service.EntityService;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.LinkedHashMap;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * Created by zajac on 23.11.2017.
 * Controller test
 */
public class BinarizationControllerTest {

    private final String id = "1L", idTwo = "2L", idThree = "3L";

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Mock
    private EntityService entityService;
    @Mock
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        this.entityService = Mockito.mock(EntityService.class);
        BinarizationController binarizationController = new BinarizationController(entityService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(binarizationController)
                .setControllerAdvice(new GlobalDefaultExceptionHandler())
                .build();
    }

    @Test
    public void singleUpload() throws Exception {
        //given
        byte[] bytes = {1, 23, 4, 53, 12, 32, 98, 67};
        String badFileName = "badName.jjppgg";
        String goodFileName = "goodName.jpg";
        ResponseEntity.badRequest().build();
        //when
        ResultActions goodAction = mockMvc.perform(
                post("/upload")
                        .param("id", id)
                        .param("filename", goodFileName)
                        .content(bytes)
        );
        ResultActions wrongFileNameAction = mockMvc.perform(
                post("/upload")
                        .param("id", idTwo)
                        .param("filename", badFileName)
                        .content(bytes)
        );
        ResultActions emptyBodyAction = mockMvc.perform(
                post("/upload")
                        .param("id", idThree)
                        .param("filename", goodFileName)
        );
        //then
        goodAction.andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.header().stringValues("Location", "/" + id));
        wrongFileNameAction.andExpect(MockMvcResultMatchers.status().isBadRequest());
        emptyBodyAction.andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void getImage() throws Exception {
        //given
        byte[] sampleBytes = {23, 12, 33, 4, 111, 122, 11, 9, 4, 4};
        ImageEntity okImageEntity = new ImageEntity("5L", "some.jpg", "", sampleBytes, null, Status.DONE.toString());
        ImageEntity processingImageEntity = new ImageEntity("5L", "some.jpg", "", null, null, Status.PROCESSING.toString());
        Mockito.when(entityService.getImageEntity(id)).thenReturn(okImageEntity);
        Mockito.when(entityService.getImageEntity(idTwo)).thenReturn(processingImageEntity);

        Response response = Response.builder()
                .status(404)
                .reason("image not found")
                .headers(new LinkedHashMap<>())
                .build();
        Exception exception = errorDecoder.decode("DATA-SOURCE#getImage(" + idThree + ")", response);
        Mockito.doThrow(exception).when(entityService).getImageEntity(idThree);
        //when
        MvcResult okMvcResult = mockMvc.perform(get("/" + id)).andReturn();
        MvcResult processingMvcResult = mockMvc.perform(get("/" + idTwo)).andReturn();
        MvcResult notFoundMvcResult = mockMvc.perform(get("/" + idThree)).andReturn();
        //then
        final int expectedOkStatus = HttpStatus.OK.value();
        final int responseOkStatus = okMvcResult.getResponse().getStatus();
        final int expectedProcessingStatus = HttpStatus.FOUND.value();
        final int responseProcessingStatus = processingMvcResult.getResponse().getStatus();
        final int expectedNotFoundStatus = HttpStatus.NOT_FOUND.value();
        final int responseNotFoundStatus = notFoundMvcResult.getResponse().getStatus();

        assertEquals(expectedOkStatus, responseOkStatus);
        assertEquals(expectedProcessingStatus, responseProcessingStatus);
        assertEquals(expectedNotFoundStatus, responseNotFoundStatus);
    }

    @Test
    public void deleteById() throws Exception {
        //given
        Exception exception = new EmptyResultDataAccessException(0);
        Mockito.doThrow(exception).when(entityService).deleteImage(idTwo);
        //when
        MvcResult okMvcResult = mockMvc.perform(delete("/" + id)).andReturn();
        MvcResult processingMvcResult = mockMvc.perform(delete("/" + idTwo)).andReturn();
        //then
        HttpStatus expectedStatusWhenOK = HttpStatus.OK;
        HttpStatus expectedStatusWhenDeleted = HttpStatus.GONE;
        assertEquals(expectedStatusWhenOK.value(), okMvcResult.getResponse().getStatus());
        assertEquals(expectedStatusWhenDeleted.value(), processingMvcResult.getResponse().getStatus());
    }

    @Test
    public void deleteAllByOriginalId() throws Exception {
        //when
        entityService.deleteAllImagesWhatContainInId(this.id);
        //then
        Mockito.verify(this.entityService, Mockito.times(1)).deleteAllImagesWhatContainInId(id);
    }
}