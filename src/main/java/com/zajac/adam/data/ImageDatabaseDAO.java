package com.zajac.adam.data;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by zajac on 05.08.2017.
 * Image data acces interface
 */
public interface ImageDatabaseDAO extends CrudRepository<ImageEntity, String> {
    @Transactional
    ImageEntity findById(String id);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "DELETE FROM image_entity WHERE id LIKE :original_id")
    void deleteAllByIdLike(@Param("original_id") String original_id);
}
