package com.zajac.adam.data;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

/**
 * Created by zajac on 05.08.2017.
 * Image entity prepared to use with database
 */
@Entity
public class ImageEntity {
    @Id
    private String id;
    private String name;
    private String description;
    @Type(type = "org.hibernate.type.MaterializedBlobType")
    private byte[] bytes;
    private Date timePoint;
    private String status;

    public ImageEntity() {
    }

    public ImageEntity(String id, String name, String description, byte[] bytes, Date timePoint, String status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.bytes = bytes;
        this.status = status;
        this.timePoint = timePoint;
    }

    public Date getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(Date timePoint) {
        this.timePoint = timePoint;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
