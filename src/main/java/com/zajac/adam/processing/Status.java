package com.zajac.adam.processing;

/**
 * Created by zajac on 07.09.2017.
 */
public enum Status {
    DONE, PROCESSING
}
