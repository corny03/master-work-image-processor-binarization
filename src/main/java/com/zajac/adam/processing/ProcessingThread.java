package com.zajac.adam.processing;

import com.zajac.adam.data.ImageDatabaseDAO;
import com.zajac.adam.data.ImageEntity;

import java.sql.Date;
import java.util.Calendar;

/**
 * Created by zajac on 07.09.2017.
 */
public class ProcessingThread implements Runnable {

    byte[] bytes;
    ImageDatabaseDAO imageDatabaseDAO;
    private ImageEntity imageEntity;

    public ProcessingThread(ImageEntity imageEntity, byte[] bytes, ImageDatabaseDAO imageDatabaseDAO) {
        this.imageEntity = imageEntity;
        this.bytes = bytes;
        this.imageDatabaseDAO = imageDatabaseDAO;
    }

    @Override
    public void run() {
        byte[] binarizeImageBytes = BinarizationProcessor.binarizeImage(this.bytes);
        this.imageEntity = prepareImageEntity(this.imageEntity, binarizeImageBytes);
        imageDatabaseDAO.save(this.imageEntity);
    }

    private ImageEntity prepareImageEntity(ImageEntity imageEntity, byte[] binarizeImageBytes) {
        Date sqlDate = getActualDate();
        imageEntity.setTimePoint(sqlDate);
        imageEntity.setBytes(binarizeImageBytes);
        imageEntity.setStatus(Status.DONE.toString());
        return imageEntity;
    }

    private Date getActualDate() {
        return new Date(Calendar.getInstance().getTime().getTime());
    }
}
