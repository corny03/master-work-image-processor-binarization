package com.zajac.adam.controller;

import feign.FeignException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by zajac on 29.11.2017.
 * global place for handling exceptions
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {

    @ExceptionHandler(value = EmptyResultDataAccessException.class)
    public ResponseEntity entityIsDeleted(EmptyResultDataAccessException e) {
        return ResponseEntity.status(HttpStatus.GONE).body("image was deleted before");
    }

    @ExceptionHandler(value = FeignException.class)
    public ResponseEntity defaultFeignHandler(FeignException e) {
        if (e.status() == 404)
            return ResponseEntity.status(e.status()).body("Not found image in data source");
        else
            return ResponseEntity.status(e.status()).body(e.getMessage());
    }
}
