package com.zajac.adam.controller;

import com.zajac.adam.data.ImageEntity;
import com.zajac.adam.processing.Status;
import com.zajac.adam.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.Objects;

/**
 * Created by zajac on 15.08.2017.
 * Main controller
 */

@Controller
public class BinarizationController {

    private EntityService entityService;

    @Autowired
    public BinarizationController(EntityService entityService) {
        this.entityService = entityService;
    }

    /**
     * @param id identifier of image
     * @return ResponseEntity with codes:
     * OK 200 - when photo is created
     * Bad Request 400 - When is bad format of photo, or body is empty
     */
    @PostMapping(Routing.UPLOAD_URL)
    public ResponseEntity<String> singleUpload(@RequestParam("id") String id,
                                               @RequestParam("filename") String fileName,
                                               @RequestBody byte[] bytes) {
        if (!isBytesOk(bytes) || !isFileNameOk(fileName) || id == null)
            return ResponseEntity.badRequest().build();
        entityService.uploadToDatabase(id, fileName, bytes);
        URI uriToBinarizedImage = prepareImageURI(id);

        return ResponseEntity.created(uriToBinarizedImage).body(id);
    }

    /**
     * @param id identifier of image
     * @return ResponseEntity with codes:
     * OK 200 - when found photo and returned
     * Found 304 - when photo is still processed or must be downloaded and processed
     * Not Found 404 - when photo is not available
     */
    @GetMapping(Routing.IMAGE_URL)
    public ResponseEntity getImage(@PathVariable String id) {
        ImageEntity imageEntity = entityService.getImageEntity(id);
        if (isImageEntityProcessing(imageEntity)) {
            return new ResponseEntity<>("In processing...", HttpStatus.FOUND);
        }
        InputStreamResource binarizedImageInputStreamResource = imageEntityToInputStreamResource(imageEntity);
        HttpHeaders responseHeaders = prepareHeader(imageEntity);
        return new ResponseEntity<>(binarizedImageInputStreamResource, responseHeaders, HttpStatus.OK);
    }

    /**
     * @param id identifier of image
     * @return ResponseEntity with codes:
     * OK 200 - when photo was deleted in this session
     * Gone 410 - when photo is not in service
     */
    @DeleteMapping(Routing.DELETE_URL)
    public ResponseEntity deleteById(@PathVariable String id) {
        entityService.deleteImage(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(Routing.DELETE_FROM_ALL_FILTERS_URL)
    public ResponseEntity deleteAllByOriginalId(@PathVariable("id") String id) {
        entityService.deleteAllImagesWhatContainInId(id);
        return ResponseEntity.ok().build();
    }

    // #######################   PRIVATE   #######################

    private boolean isBytesOk(byte[] bytes) {
        return bytes != null && bytes.length > 1;
    }

    private URI prepareImageURI(String id) {
        String path = Routing.IMAGE_URL.split("[{]")[0];
        path += id;
        return URI.create(path);
    }

    private boolean isFileNameOk(String fileName) {
        return (fileName.toLowerCase().contains(".jpg")
                || fileName.toLowerCase().contains(".jpeg")
                || fileName.toLowerCase().contains(".png"));
    }

    private boolean isImageEntityProcessing(ImageEntity imageEntity) {
        return Objects.equals(imageEntity.getStatus(), Status.PROCESSING.name());
    }

    private HttpHeaders prepareHeader(ImageEntity imageEntity) {
        String fileName = imageEntity.getName();
        byte[] imageBytes = imageEntity.getBytes();
        HttpHeaders respHeaders = new HttpHeaders();
        if (fileName.toLowerCase().contains("jpeg") || fileName.toLowerCase().contains("jpg"))
            respHeaders.setContentType(MediaType.IMAGE_JPEG);
        else if (fileName.toLowerCase().contains("png"))
            respHeaders.setContentType(MediaType.IMAGE_PNG);
        else throw new InternalError("Wrong media type");
        respHeaders.setContentLength(imageBytes.length);
        respHeaders.setContentDispositionFormData("attachment", fileName);

        return respHeaders;
    }

    private InputStreamResource imageEntityToInputStreamResource(ImageEntity imageEntity) {
        byte[] imageBytes = imageEntity.getBytes();
        InputStream inputStream = new ByteArrayInputStream(imageBytes);
        return new InputStreamResource(inputStream);
    }
}

